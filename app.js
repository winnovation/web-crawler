var express = require('express');
var app = express();
var crawlerObj = require('./crawler');
var finder = require('./finder');
var path = require('path');
var googleTrends = require('google-trends-api');

var conf = require('./conf');
var mongo = require('./mongo');


const urlTool = require('url');

app.use(express.static('views/res'));



app.get(['/queryDomain/', '/queryDomain.html'], function (req, res) {
	var keyword = req.query.keyword;
	var domain = req.query.domain;
	console.log(domain);
	var crawler = crawlerObj.createCrawler(domain);
    crawler.crawl(function(domainName){
		mongo.findInDomain(domainName, keyword, function(links){
                res.send(JSON.stringify(links));

        });
	});

});

app.get(['/query/', '/query.html/'], function(req, res){
	res.sendFile('views/query.html', {root: __dirname});
});

app.get(['/googleTrends/:keyword', '/googletrends.html/'], function(req, res){
    googleTrends.trendData(req.params.keyword).then(function(result){
        res.status(200).send(JSON.stringify(result));
    });
})

app.get('/test/', function (req, res){

	crawlerObj.freezeCrawlers(function(){
		res.send();
		process.exit();
	});
    // console.log(urlTool.parse("http://www.craveonline.com").host);
	/*conf.loadParams(function(){
		console.log(conf.getParam("crawlerLimit"));
		console.log(conf.getParam("pageIsObsoleteIn"));
	});*/
});

var port = (process.env.VCAP_APP_PORT || 3000);
// var host = (process.env.VCAP_APP_HOST || 'localhost');
if(process.argv[2] != undefined)
	port = process.argv[2];

app.listen(port, function () {

    console.log('Example app listening on port ' + port + '!');
    conf.loadParams(function () {
    });
    mongo.connect(function () {
        console.log("Connected to mongodb");
        // crawlerObj.defrostCrawlers();
    });
});

/*
process.on('SIGINT', function() {
    console.log("Caught interrupt signal");
	crawlerObj.freezeCrawlers(function(){
        process.exit();
	});
});*/
