	var fs = require('fs');


	var confObject;
	var fileName = "config.json";
	module.exports = {
		loadParams: function(callback){
			readFile(callback);
		},
		getParam : function(name) {
			return confObject[name];
		}
	}

	var readFile = function(callback){
		fs.readFile(__dirname + '/' + fileName, 'utf8', function (err,data) {
		  if (err) {
			console.log("Error in reading file");
		  }
		  	confObject = JSON.parse(data);
			callback();
		});
	}
