var MongoClient = require('mongodb').MongoClient;
var MongoObjectId = require('mongodb').ObjectID;

var assert = require('assert');
var fs = require('fs');
var finder = require('./finder');


const mongoUrl = "mongodb://admin:YGTNPAZOJMGEVLQX@sl-eu-lon-2-portal.3.dblayer.com:15827/crawler?authSource=admin";


var db;
var options = {
    mongos: {
        ssl: true,
        sslValidate: false,
    }
}



module.exports = {
    connect: function(callback){
        MongoClient.connect(mongoUrl, options, function(err, mongoDB) {
            if(err){
                console.log("Error: " + err);
            }
            db = mongoDB;
            callback();
        });
    },
    getWebsiteDate: function(domainName, callback){
        db.collection('websites').findOne({"domain": domainName}, {"time": 1}, function(err, document){
            if (err) {
                console.log(err);
                callback(null);
            } else if (document) {
                callback(document.time);
            } else {
                console.log('No document(s) found with defined "find" criteria!');
                callback(null);
            }
        });
    },
    domainExists: function(domainName, callback) {
        db.collection('websites').find({"domain": domainName}, {"domain": 1}).toArray(function (err, result) {
            if (err) {
                console.log(err);
                callback(null);
            } else if (result.length > 0) {
                callback(true);
            } else {
                callback(false);
            }
        });
    },
    addWebsite: function(data, callback) {
        db.collection('websites').update({"domain": data.domain}, {$set: data}, {upsert: true}, function(err, result){
            if(err == null)
                callback(true);
            else{
                console.log(err);
                callback(false);
            }
        });
    },
     addCrawlerQueue: function(id, data, callback) {
        db.collection('queues').update({"id": id}, {$set: {"queue": data}}, {upsert: true}, function(err, result){
            if(err == null)
                callback(true);
            else{
                console.log(err);
                callback(false);
            }
        });
    },
    getCrawlerQueues: function(callback) {
        db.collection('queues').find().toArray(function(err, result){
            callback(result);
        });
    },
    addLinkToDomain: function(domainName, link, callback) {
        var toInsert = {url: link.url, page: link.page};
        db.collection('pages').insertOne(toInsert, function(err, record){
            if(err)
                callback(false);
            else{
                db.collection('websites').update({"domain": domainName}, {$push: {urls: toInsert._id}}, {upsert: true}, function(err, result){
                    if(err)
                        callback(false);
                    callback(true);
                });
            }
        });
    },
    findInDomain: function(domainName, keyword, callback) {
            db.collection('keywords').findOne({"keyword": keyword.toLowerCase(), "domain": domainName}, function(err, document){
                if (err) {
                    console.log(err);
                    callback(null);
                } else if (document) {
                    callback(document.urls);
                } else {
                    var resultUrls = [];
                    // db.collection('pages').find({"url": {$regex: "/^" + domainName + "/", $options: "i"}, "page": {$regex: "/" + keywords[j] + "/", $options: "i"}}, {"url": 1}).toArray(function (err, result) {
                    var urlRegex = new RegExp(domainName);
                    var pageRegex = new RegExp(keyword);
                    db.collection('pages').find({"url": {$regex: urlRegex, $options: "i"}, "page": {$regex: pageRegex, $options: "i"}}, {"url": 1}).toArray(function (err, result) {
                        if(result != undefined)
                            for(i = 0; i < result.length; i++)
                                resultUrls.push(result[i].url);
                        saveKeywordLinks(domainName, keyword, resultUrls, function (success) {
                            callback(resultUrls);
                        })
                    });
                }
            });
    }
}

var saveKeywordLinks = function(domainName, keyword, links, callback) {
    db.collection('keywords').update({"keyword": keyword.toLowerCase(), "domain": domainName}, {$addToSet: {urls: {$each: links}}}, {upsert: true}, function(err, result){
        if(err == null)
            callback(true);
        else{
            console.log(err);
            callback(false);
        }
    });
}

