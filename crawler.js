
const urlTool = require('url');
var cheerio = require('cheerio');
var fs = require('fs');
var conf = require('./conf');
var mongo = require('./mongo');

var Crawler = require("simplecrawler");

var crawlersList = [];

module.exports = {
	createCrawler: function(url) {
		return new MyCrawler(url, null);
	},
    freezeCrawlers: function(callback) {
        function repeater(j){
            if(j < crawlersList.length){
                crawlersList[j].queue.freeze('frozencrawlers/' + j, function(){
                    fs.readFile('frozencrawlers/' + j, 'utf8', function (err, queueData) {
                        if (err) {
                            return console.log(err);
                        }
                        else{
                            mongo.addCrawlerQueue(j, queueData, function(){
                                repeater(j + 1);
                            });
                        }
                    });

                });
            }
            else
                callback();
        }
        repeater(0);
    },
    defrostCrawlers: function(){
	    mongo.getCrawlerQueues(function(queues){
	        for(i = 0; i < queues.length; i++) {
	            var queue = queues[i].queue
                fs.writeFileSync("frozencrawlers/" + i, queue);
            }
            fs.readdir("frozencrawlers", function (err, files) {
                files.forEach(function(file) {
                    fs.readFile("frozencrawlers/" + file, 'utf8', function (err, data) {
                        if (err) {
                            return console.log(err);
                        }
                        else{
                            var url = JSON.parse(data)["0"].host;
                            var c = new MyCrawler(url, file);
                            c.initMyCrawler();
                        }
                    });
                });
            });
        });
    }
}


function MyCrawler(url, crawlerQueueId) {

    this.crawlerQueueId = crawlerQueueId;
    this.mainUrl = arrangeDomain(url);
    console.log("Main url: " + this.mainUrl);
    this.domain = urlTool.parse(arrangeDomain(url)).host;
    this.crawler;

    this.crawl = function (callback) {
        console.log("Domain to crawl: " + this.domain);
        var myself = this;
        this.initMyCrawler();

        domainExists(this.domain, function (exists) {
            if (exists) {
                isWebsiteDataUpdated(myself.domain, function (isUpdated, date) {
                    if (!isUpdated) {
                        console.log("Previous record exists but data is more than " + conf.getParam("pageIsObsoleteIn") + " days old. Gonna crawl the website");
                        mysef.crawlInit(myself.mainUrl, function () {
                            callback(myself.domain, false);
                        });
                    }
                    else {
                        console.log("Previous record and data is more recent than " + conf.getParam("pageIsObsoleteIn") + " days. Gonna access cached data");
                        callback(myself.domain, true);
                    }
                });
            }
            else
                myself.crawlInit(myself.mainUrl, function () {
                    callback(myself.domain, false);
                });
        });
    }

    this.initMyCrawler = function () {
        var myself = this;
        this.crawler = new Crawler(this.mainUrl);
        crawlersList.push(this.crawler);
        this.crawler.maxConcurrency = 20;
        var filesCondition = this.crawler.addFetchCondition(function (queueItem, referrerQueueItem) {
            return !queueItem.path.match(/(\.(pdf|mp3|mp4|doc|docx|css|js|jpg|jpeg|png|gif|xml)|(wp\-admin|wp\-json))/i);
        });
        // crawler.scanSubdomains = true;
        // this.crawler.cache = new Crawler.cache('');
        if(this.crawlerQueueId != null){
            myself.crawlWebsite(function(){
                console.log("Finished");
            });
        }
    }

    this.crawlInit = function (url, callback) {
        var toSend = {};
        toSend.domain = this.domain;
        toSend.time = new Date().getTime();
        toSend.urls = [];
        var myself = this;
        exportToMongo(toSend, function (success) {
            if (success)
                console.log("Added domain to mongo");
            else
                console.log("Error adding domain to mongo");
            myself.crawlWebsite(function () {
                callback();
            });
        });
    }


    this.crawlWebsite = function (callbackFunction) {
        var myself = this;
        this.crawler.on('fetchcomplete', function (queueItem, responseBuffer, response) {
            var page = {};
            page.url = queueItem.url;
            page.page = returnTextContent(responseBuffer.toString('utf-8'));
            mongo.addLinkToDomain(myself.domain, page, function (success) {
                console.log("Added link to " + myself.domain + ": " + queueItem.url);
                // myself.crawler.queue[queueItem.id] = null;
            });
        });
        this.crawler.on('complete', callbackFunction);
        this.crawler.on('crawlstart', function(){
            console.log("Started crawler for domain " + myself.domain);
        });
        var myself = this;
        if(this.crawlerQueueId != null){
            this.crawler.queue.defrost("frozencrawlers/" + this.crawlerQueueId, function(err, queue){
                myself.crawler.start();
            });
        }
        else
            myself.crawler.start();
    }
}

var isWebsiteDataUpdated = function(domain, callback){
    mongo.getWebsiteDate(domain, function(date){
        var websiteDataTime = new Date(date);
        var nowdate = new Date();

        var millisecondsPerDay = 1000 * 60 * 60 * 24;
        var days = (nowdate.getTime() - websiteDataTime.getTime()) / millisecondsPerDay;

        if(Math.floor(days) > conf.getParam("pageIsObsoleteIn")){
            callback(false, null);
        }
        else
            callback(true, date);
    });


}

var exportToMongo = function(data, callback) {
    mongo.addWebsite(data, callback);
}

var domainExists = function(domain, callback) {
    mongo.domainExists(domain, callback);

}

var returnTextContent = function (html) {
    var text = "";
    var $ = cheerio.load(html);
    $("p, h1, h2, h3, h4, h5").each(function(){
        if(!$(this).parent().is("a")){
            text += $(this).text() + " ";
        }
    });
    text = text.replace(/[^\w ]/gi, '');
    return text;
}


var arrangeDomain = function(url){
    if(url.substring(0, 7) != "http://" && url.substring(0, 8) != "https://")
        return "http://" + url;
    return url;
}

var arrayContains = function(value, array) {
    return array.indexOf(value) > -1;
}