


var fs = require('fs');


module.exports = {
    searchInTextArray: function(textArray, keywordArray) {
        var searchResults = [];
        if(textArray != undefined && textArray.length != 0){
            for(j = 0; j < textArray.length; j++){
                var result = searchFunction(textArray[j], keywordArray);
                // console.log(result);
                searchResults.push(result);
            }
        }
        return searchResults;
    },
    searchInText: function(text, keywordArray) {
        return  searchFunction(text, keywordArray);
    }
}



var searchFunction = function(text, keywordArray){
    for(i = 0; i < keywordArray.length; i++){
        var currentText = text.toLowerCase();
        if(currentText.indexOf(keywordArray[i]) != -1)
            return true;
    }

    return false;
}